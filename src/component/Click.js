import { Component } from "react";

class Click extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }

        // C1: Sử dụng bind để trỏ this về class
        //this.onBtnClickHandler = this.onBtnClickHandler.bind(this);

        //C2: Sử dụng Arrow function( do arrow function không có this, nên this mặc định trỏ về class)
    }
    onBtnClickHandler = () => {
        console.log("Click here!");
        this.setState({
            count: this.state.count + 1
        })
    }
    onBtnClickHandlerDown = () => {
        console.log("Click here!");
        this.setState({
            count: this.state.count - 1
        })
    }
    render() {
        return(
            <div className="container">
                <h1>Count click: {this.state.count}</h1>
                <hr/>
                <button className="button" onClick={this.onBtnClickHandler}>Up</button>

                <button className="button" onClick={this.onBtnClickHandlerDown}>Down</button>
            </div>
        )
    }
}
export default Click;